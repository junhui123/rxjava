package Chapter8;

import common.CommonUtils;
import common.GsonHelper;
import common.Log;
import common.OkHttpHelper;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

@RunWith(JUnitPlatform.class)
public class TestAsyncExample {
    @DisplayName("test Observable.interval() wrong")
    @Test
    //@Disabled //@Ignore와 동일
    void testIntervalWrongWay(){
        /**
         * Expected: 5 [0, 1, 2, 3, 4], Actual: 0 [] (latch = 1, values = 0, errors = 0, completions = 0)
         * interval()은 계산 스케쥴러에서 실행 되므로 테스트 실패
         */
        Observable<Integer> source = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .take(5)
                .map(Long::intValue);

        source.doOnNext(Log::d)
                .test().assertResult(0, 1, 2, 3, 4);
    }

    @DisplayName("test Observable.interval()")
    @Test
    void testInterval() {
        /**
         * awaitDone()
         * test()를 사용하는 스레드에서 onComplete() 호출 때 까지 대기시켜
         * 결과 확인에 도움
         */
        Observable<Integer> source = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .take(5)
                .map(Long::intValue);

        source.doOnNext(Log::d)
                .test()
                .awaitDone(1L, TimeUnit.SECONDS)
                .assertResult(0,1,2,3,4);
    }

    @DisplayName("test Github v3 API on HTTP")
    @Test
    void testHttp() {
        final String url = "https://api.github.com/users/yudong80";
        Observable<String> source = Observable.just(url)
                .subscribeOn(Schedulers.io())
                .map(OkHttpHelper::get)
                .doOnNext(Log::d)
                .map(json -> GsonHelper.parseValue(json,"name"))
                .observeOn(Schedulers.newThread());

        String expected = "Dong Hwan Yu";
        source.doOnNext(Log::i)
                .test()
                .awaitDone(3, TimeUnit.SECONDS)
                .assertResult(expected);    }
}
