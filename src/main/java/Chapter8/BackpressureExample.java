package Chapter8;

import common.CommonUtils;
import common.Log;
import io.reactivex.BackpressureOverflowStrategy;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class BackpressureExample {
    public static void main(String[] args) {
        //배압 이슈 : 데이터가 너무 많으면 효율이 떨어지는 현상(메모리 사용량에 비해서 처리 효율이 떨어짐, Out Of Memory 예외 발생)
        //makeBackPressure();
        usingBuffer();
        //usingDrop();
        //usingLatest();
    }

    public static void makeBackPressure(){
        CommonUtils.exampleStart();

        //뜨거운 Observable, 데이터 발행 속도와 처리 속도 차이 보호 제공 없음
        PublishSubject<Integer> subject = PublishSubject.create();
        subject.observeOn(Schedulers.computation()) //계산 스케쥴러에서 결과 데이터 출력
                .subscribe(data->{
                    CommonUtils.sleep(100);
                    Log.it(data);
                }, err->Log.e(err.toString())); //

        for(int i=0; i<50_000_0000; ++i){
            subject.onNext(i);
        }
        subject.onComplete();
    }

    public static void usingBuffer(){
        /**
         * 버퍼 사용
         * onBackPressureBuffer(boolean delayError)
         * delayError(true인 경우 예외 발생 시 버퍼 쌓인 데이터 처리시 까지 예외 던지지 않음, false는 바로 예외를 던짐, 기본값 false)
         *
         * onBackPressureBuffer(long capacity, Action onOverflow, BackPressureOverflowStrategy overflowStrategy)
         * 버퍼 개수(128 기본) 지정
         * onOverflow로 버퍼가 넘칠때 동작
         * strategy는 버퍼가 가득 찬 경우 실행 전략
         *   ERROR : MissingBackPressureException 예외 던지고 데이트 흐름 중단
         *   DROP_LATEST : 버퍼에 쌓여 있는 최근값 제거
         *   DROP_OLDEST : 버퍼에 쌓여 있는 오래된 값을 제거
         */
        CommonUtils.exampleStart();
        Flowable.range(1, 50_000_000)
                 //데이터 발행 속도가 빠른 경우 128개의 버퍼로 대응 무리
                .onBackpressureBuffer(128, ()->{}, BackpressureOverflowStrategy.DROP_OLDEST)
                .observeOn(Schedulers.computation())
                .subscribe(data->{
                    CommonUtils.sleep(100);
                    Log.it(data);
                }, err->Log.e(err.toString()));
    }

    public static void usingDrop(){
        CommonUtils.exampleStart();

        //버퍼가 가득 찼을 때 이후 데이터 무시
        //버퍼에 128개 데이터가 가득차 계산 스케쥴러에서 출력하기도 전에 종료
        //버퍼에 데이터가 가득 차기를 기다리는 대기 시간 필요
        Flowable.range(1, 50_000_000)
                .onBackpressureDrop()
                .observeOn(Schedulers.computation())
                .subscribe(data->{
                    CommonUtils.sleep(100);
                    Log.it(data);
                }, err->Log.e(err.toString()));

        //CommonUtils.sleep(5000); //버퍼가 차기를 기다리는 시간. 5초 안에 버퍼에 데이터가 가득 차지 않아도 종료
        CommonUtils.sleep(20_000); //버퍼가 차기를 기다리는 시간
    }

    public static void usingLatest(){
        CommonUtils.exampleStart();

        //마지막 값을 발행 보장
        //버퍼가 가득 차면
         Flowable.range(1, 50_000_000)
                .onBackpressureLatest()
                .observeOn(Schedulers.computation())
                .subscribe(data->{
                    CommonUtils.sleep(100);
                    Log.it(data);
                }, err->Log.e(err.toString()));

        CommonUtils.sleep(20_000);
    }
}
