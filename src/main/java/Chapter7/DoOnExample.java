package Chapter7;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import java.util.concurrent.TimeUnit;

public class DoOnExample {
    public static void main(String[] args) {
        //basic();
        //withError();
        //doOnEach();
        //doOnEachObserver();
        doOnSubscribeAndDispose();
        System.out.println();
        doOnLifeCycle();
        System.out.println();
        doOnTerminate();
    }

    public static void basic(){
        /**
         * onNext, onComplete, 이벤트 do가 붙은 메소드를 통해 중간에 발생하는 데이터 확인
         * onError는 doOnComplete를 통해 예외 처리
         *
         * 메소드 순서는 상관없음
         */
        String[] orgs = {"1", "3" , "5"};
        Observable<String> source = Observable.fromArray(orgs);
        source.doOnError(e->Log.e("onError", e.getMessage()))
              .doOnNext(data->Log.d("onNext()", data))
              .doOnComplete(()->Log.d("onComplete()"))
              .subscribe(Log::i);
    }

    public static void withError(){
        Integer[] divider = {10,5,0};

        Observable.fromArray(divider)
                .map(div->1000/div)
                .doOnComplete(()->Log.d("onComplete()"))
                .doOnError(e->Log.e("onError", e.getMessage()))
                .subscribe(Log::i);
    }

    public static void doOnEach(){
        /**
         * onNext, onComplete, onError 이벤트를 한 번에 처리
         */
        String[] data = {"ONE", "TWO", "THREE"};
        Observable<String> source = Observable.fromArray(data);

        //내부에서 순서는 상관 없음
        source.doOnEach(noti->{
            if(noti.isOnError()) Log.e("onNext", noti.getError().getMessage());
            if(noti.isOnNext()) Log.d("onNext", noti.getValue());
            if(noti.isOnComplete()) Log.d("onComplete");
        }).subscribe(System.out::println);
    }

    public static void doOnEachObserver() {
        String[] orgs = {"1", "3", "5"};
        Observable<String> source = Observable.fromArray(orgs);

        source.doOnEach(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
                //doOnEach 내부에서 onSubscribe는 호출되지 않음
            }

            @Override
            public void onNext(String s) {
                Log.d("onNext", s);
            }

            @Override
            public void onError(Throwable e) {
                Log.e("onNext", e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.d("onComplete");
            }
        }).subscribe(Log::i);
    }

    public static void doOnSubscribeAndDispose(){
        /**
         * doOnSubscribe = Observable을 구독했을 때 어떤 작업, Disposable(구독을 의미) 객체 생성
         * doOnDispose() = 구독 해지 시 호출. Action 객체를 인자로 받음
         *
         * Action 클래스는 doOnComplete와 doOnDispose 같은 인자 없는 람다 표현식 넣어야 할 때 사용
         */
        String[] orgs = {"1", "3", "5", "2", "6"};
        Observable<String> source = Observable.fromArray(orgs)
                //zipWith으로 합성하여 계산 스케쥴러에서 작동
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a,b)->a)
                .doOnSubscribe(d->Log.d("onSubscribe"))
                .doOnDispose(()->Log.d("onDispose()"));
                //.doOnSubscribe(d->Log.d("d value "+d)) //여러개 등록 가능하나 값이 달라짐
                //.doOnDispose(()->Log.d("onDispose()"));

        Disposable disposable = source.subscribe(Log::i);

        CommonUtils.sleep(200);
        disposable.dispose();
        CommonUtils.sleep(300);
    }

    public static void doOnLifeCycle() {
        String[] orgs = {"1", "3", "5", "2", "6"};
        Observable<String> source = Observable.fromArray(orgs)
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a,b)->a)
                .doOnLifecycle(d->Log.d("onSubscribe()"), ()->Log.d(("onDisplose()")));

        Disposable disposable = source.subscribe(Log::i);

        CommonUtils.sleep(200);
        disposable.dispose();
        CommonUtils.sleep(300);
    }

    public  static void doOnTerminate() {
        /**
         * doOnTerminate()는 onComplete, onError 이벤트가 발생하여 Observable이 종료되는 이벤트가 발생 직전에 호출
         */
        String[] orgs = {"1", "3", "5"};
        Observable<String> source = Observable.fromArray(orgs);

        source.doOnTerminate(()->Log.d("onTerminate"))
                .doOnComplete(()->Log.d("onComplete"))
                .doOnError(e->Log.e("onError", e.getMessage()))
                .subscribe(Log::i);
    }
}
