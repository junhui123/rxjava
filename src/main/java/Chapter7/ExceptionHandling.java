package Chapter7;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.schedulers.Schedulers;

public class ExceptionHandling {
    public static void main(String[] args) {
        //cannotCatch();
        System.out.println();
        //onErrorReturn();
        System.out.println();
        onErrorResumeNext();
    }

    public static void cannotCatch(){
        Observable<String> source = Observable.create((ObservableEmitter<String> emitter)->{
            emitter.onNext("1");
            emitter.onError(new Exception("Some Error"));
            emitter.onNext("3");
            emitter.onComplete();
        });

        try{
            source.subscribe(Log::i);
        } catch(Exception e) {
            Log.e(e.getMessage());
        }
    }

    public static void onErrorReturn() {
        /**
         * onError를 사용하면 구독자가 subscribe 예외 처리를 해야 함
         * 구독자가 예외처리를 파악하기는 어려움
         *
         * Observable에서 예외 처리를 미리 해두면 구독자가 알맞는 처리를 할 수 있음
         */
        String[] grades = {"70", "88", "$100", "93", "83"};
        Observable<Integer> source = Observable.fromArray(grades)
                .map(data->Integer.parseInt(data))
                .onErrorReturn(e->{
                    if(e instanceof NumberFormatException){
                        e.printStackTrace();
                    }
                    return -1;
                });

        source.subscribe(data->{
            if(data<0) {
                Log.e("Wrong Data Found()");
                return;
            }
            Log.i("Grade is "+ data);
        });
    }

    public static void onErrorResumeNext(){
        /**
         * onErrorReturn과 같으나 에러가 발생 시 내가 원하는 Observable로 대체
         */
        String[] salesData = {"100", "200", "A300"};
        //defer()는 Observable(데이터) 생성을 subscribe() 까지 미룸
        Observable<Integer> onParseError = Observable.defer(()->{
           Log.d("send email to admin");;
           return Observable.just(-1);
        }).subscribeOn(Schedulers.io());

        Observable<Integer> source = Observable.fromArray(salesData)
                .map(Integer::parseInt)
                .onErrorResumeNext(onParseError);

        source.subscribe(data->{
            if(data < 0) {
                Log.e("Wrong Data Found");
                return;
            }
            Log.i("Sales data : "+ data);
        });

        CommonUtils.sleep(1000);
    }
}
