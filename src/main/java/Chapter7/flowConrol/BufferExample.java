package Chapter7.flowConrol;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class BufferExample {
    public static void main(String[] args) {
        useBuffer();
    }

    public static void useBuffer(){
        /**
         * 일정 시간 간격 동안 데이터를 모은후 한번에 발행
         * 데이터 흐름이 넘치는 경우 활용
         */
        String[] data = {"1", "2", "3", "4", "5", "6"};

        //앞의 3개는 100ms 간격으로 발행
        Observable<String> earlySource = Observable.fromArray(data)
                .take(3)
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a,b)->a);

        //가운데 1개는 300ms 후에 발행
        Observable<String> middleSource = Observable.just(data[3])
                .zipWith(Observable.timer(300L, TimeUnit.MILLISECONDS), (a,b)->a);

        //마지막 2개는 100ms 후에 발행
        Observable<String> lastSource = Observable.just(data[4], data[5])
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a,b)->a);

        //3개씩 모아서 한꺼번에 발행
        Observable<List<String>> source = Observable.concat(earlySource, middleSource, lastSource)
                .buffer(3);
                //.buffer(2, 3); buffer(skip, count) skip>count. 2개 데이터 발행되면 다음 1개는 건너뜀

        source.subscribe(Log::i);
        CommonUtils.sleep(1000);

    }
}
