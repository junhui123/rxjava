package Chapter7.flowConrol;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class WindowEXample {
    public static void main(String[] args) {
        useWindow();
    }

    public static void useWindow(){
        /**
         * groupBy()는 조건에 맞는 입력값을 Observable<GroupedObservable<K,T>>을 리턴하여 병렬적으로 Observable을 생성
         * window(count)는 Observable<Observable<T>>를 리턴하여 차례로 여러 Observable을 만듬
         * window(count)는 throttle과 sample 처럼 일부의 값들만 받아 들일 수 있음
         * window(timespan, timeskip, unit)은 필터링 작업을 해야 하므로 계산 스케쥴러를 활용
         * window(count)는 입력된 값을 그대로 발행하므로 비동기 작업이 아니므로 현재 스레드 그대로 사용
         */

        String[] data = {"1", "2", "3", "4", "5", "6"};
        CommonUtils.exampleStart();

        //앞의 3개는 100ms 간격으로 발행
        Observable<String> earlySource = Observable.fromArray(data)
                .take(3)
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a, b) -> a);

        //가운데 1개는 300ms 후에 발행
        Observable<String> middleSource = Observable.just(data[3])
                .zipWith(Observable.timer(300L, TimeUnit.MILLISECONDS), (a,b) -> a);

        //마지막 2개는 100ms 후에 발행
        Observable<String> lateSource = Observable.just(data[4], data[5])
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a,b) -> a);

        //3개씩 모아서 새로운 옵저버블을 생성함
        Observable<Observable<String>> source = Observable.concat(
                earlySource,
                middleSource,
                lateSource)
                .window(3);

        source.subscribe(observable -> {
            Log.dt("New Observable Started!!");
            observable.subscribe(Log::it);
        });
        CommonUtils.sleep(1000);
    }
}
