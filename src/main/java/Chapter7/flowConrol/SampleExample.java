package Chapter7.flowConrol;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class SampleExample {
    public static void main(String[] args) {
        useSample();
    }

    public static void useSample(){
        /**
         * 특정 시간 동안 발행한 데이터가 아무리 많아도
         * 마지막 데이터만 발행하고 나머지 무시
         */

        String[] data = {"1", "7", "2", "3", "6"};

        CommonUtils.exampleStart();

        //앞의 4개는 100ms 간격으로 발행
        Observable<String> earlySource = Observable.fromArray(data)
                .take(4)
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a,b)->a);
//        earlySource.subscribe(d->{
//            System.out.println("earlySource : "+ d);
//        });

        //마지막 데이터는 300ms 후에 발행
        Observable<String> lastSource = Observable.just(data[4])
                .zipWith(Observable.interval(300L, TimeUnit.MILLISECONDS), (a,b)->a);
//        lastSource.subscribe(d->{
//            System.out.println("lastSource : "+ d);
//        });

        //2개의 Observable을 합치고 300ms로 샘플링
        Observable<String> source = Observable.concat(earlySource, lastSource)
                .sample(300L, TimeUnit.MILLISECONDS);
                //.sample(300L, TimeUnit.MILLISECONDS, true); //마지막 값을 발행 설정 emitLast true

        source.subscribe(Log::it);
        CommonUtils.sleep(1000);
    }
}
