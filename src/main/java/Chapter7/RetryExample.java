package Chapter7;

import common.CommonUtils;
import common.Log;
import common.OkHttpHelper;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class RetryExample {
    public static void main(String[] args) {
        useRetry();
    }

    public static void useRetry(){
        /**
         * 바로 다시 또는 일정 시간을 두고 subscribe() 호출하여 재구독
         *
         * 테스트를 위해서는 인터넷 접속 해제 필요
         */
        final int RETRY_MAX = 5;
        final int RETRY_DELAY = 1000;

        CommonUtils.exampleStart();

        String url = "https://api.github.com/zen";
        Observable<String> source = Observable.just(url)
                .map(OkHttpHelper::getT)
                //.retry(5)
                .retry((retryCnt, e) -> {
                    Log.e("retryCnt = " + retryCnt);
                    CommonUtils.sleep(RETRY_DELAY);

                    return retryCnt < RETRY_MAX ? true: false;
                })
                .onErrorReturn(e -> CommonUtils.ERROR_CODE);

        source.subscribe(data -> Log.it("result : " + data));
        CommonUtils.exampleComplete();
    }

    public static void retryUntil() {
        /**
         * 특정 조건이 만족되어야지 재시도
         */
        CommonUtils.exampleStart();

        String url = "https://api.github.com/zen";
        Observable<String> source = Observable.just(url)
                .map(OkHttpHelper::getT)
                .subscribeOn(Schedulers.io())
                .retryUntil(() -> {
                    if(CommonUtils.isNetworkAvailable())
                        return true; //stop

                    CommonUtils.sleep(1000);
                    return false; //continue
                });
        source.subscribe(Log::i);

        CommonUtils.sleep(5000);
        CommonUtils.exampleComplete();
    }

    public static void retryWhen() {
        /**
         * 재시도 조건을 동적으로 설정하는 복잡한 로직 구현시 사용
         * 아래는 재시도를 하되 횟수가 늘어날때 마다 재시도 시간이 늘어나도록 구현됨
         */
        Observable.create((ObservableEmitter<String> emitter) -> {
            System.out.println("subscribing");
            emitter.onError(new RuntimeException("always fails"));
        }).retryWhen(attempts -> {
            return attempts.zipWith(Observable.range(1, 3), (n, i) -> i).flatMap(i -> {
                System.out.println("delay retry by " + i + " second(s)");
                return Observable.timer(i, TimeUnit.SECONDS);
            });
        }).blockingForEach(System.out::println);
        CommonUtils.exampleComplete();
    }
}
