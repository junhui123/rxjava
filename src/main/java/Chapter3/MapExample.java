package Chapter3;

import common.Log;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class MapExample {
    public static void main(String[] args) {
        useMap();
        System.out.println();
        useMapWithFunction();
        System.out.println();
        mappingType();
        System.out.println();
        useflatMap();
    }

    private static void useMap() {
        String[] balls = {"1","2","3","5"};
        Observable<String> source = Observable.fromArray(balls)
                .map(ball-> ball+"<>");
        source.subscribe(Log::i);
    }

    private static void useMapWithFunction() {
        Function<String, String> getDiamond = ball -> ball+"<>";
        String[] balls = {"1","2","3","5"};
        Observable<String> source = Observable.fromArray(balls)
                .map(getDiamond);
        source.subscribe(Log::i);
    }

    private static void mappingType() {
        Function<String, Integer> ballToIndex = ball -> {
            switch (ball) {
                case "RED": return 1;
                case "YELLOW": return 2;
                case "GREEN": return 3;
                case "BLUE": return 5;
                default: return -1;

            }
        };
        String[] balls = {"RED","YELLOW","GREEN","BLUE"};
        Observable<Integer> source = Observable.fromArray(balls)
                .map(ballToIndex);
        source.subscribe(Log::i);
    }

    private static void useflatMap() {
        //Observable을 사용한 여러개의 데이터 발행
        //String을 넣으면 여러 개의 Observable<String>이 나온다는 의미...
        Function<String, Observable<String>> getDoubleDiamonds = ball-> Observable.just(ball+"<>", ball+"<>");
        String[] balls = {"1", "3", "5"};

        Observable<String> source = makeFlatMapSource(balls, getDoubleDiamonds);
        source.subscribe(Log::i);

        Observable<String> source2 = Observable.fromArray(balls).flatMap(ball->Observable.just(ball+"<>", ball+"<>"));
        source2.subscribe(Log::i);
    }

    private static <T> Observable<T> makeMapSource(T data, Function<T, T> f) {
        Observable<T> source = Observable.fromArray(data)
                .map(f);
        return source;
    }

    private static <T> Observable<T> makeFlatMapSource(T[] data, Function<T, Observable<T>> f) {
        //flatMap은 처리 도중 새로운 데이터가 들어오면 나중에 들어온 데이터의 처리 결과가 먼저 출력 될 수 있음
        Observable<T> source = Observable.fromArray(data)
                .flatMap(f);
        return source;
    }
}
