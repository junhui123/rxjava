package Chapter3;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;


public class ReduceExample {
    public static void main(String[] args) {
        //map에서 function 적용 데이터를 filter에서 predicate를 통해 걸러내고
        //reduce에서 BiFunction을 적용 하여 최종 결과 데이터 합성

        //Observable을 통해서 들어오는 데이터를 1개씩 모아서 최종 결과를 생성
        biFunction();
    }

    public static void biFunction() {
        BiFunction<String, String, String> mergeBalls = (ball1, ball2) -> ball2 + "(" + ball1 +")";

        String[] balls  = {"1", "3", "5"};
        //reduce 수행 하여 결과과 0개일 수도 있음
        Maybe<String> source = Observable.fromArray(balls)
                .reduce(mergeBalls);
        source.subscribe(System.out::println);
    }
}
