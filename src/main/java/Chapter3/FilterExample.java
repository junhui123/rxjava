package Chapter3;

import io.reactivex.Observable;
import io.reactivex.Single;

public class FilterExample {
    public static void main(String[] args) {
        Integer[] numbers = {100,200,300,400,500};
        Single<Integer> single;
        Observable<Integer> source;

        //아무 값없이 Observable이 완료되면 기본값을 대신 발행
        //1. first
        single = Observable.fromArray(numbers).first(-1);
        single.subscribe(data->System.out.println("first() value = "+ data));

        //last는 onComplete이전 호출한 onNext 이벤트를 의미
        //아무 값없이 Observable이 완료되면 기본값을 대신 발행
        //2. last
        single = Observable.fromArray(numbers).last(999);
        single.subscribe(data->System.out.println("last() value = "+ data));

        //3. take
        source = Observable.fromArray(numbers).take(3);
        source.subscribe(data->System.out.println("take(3) values = "+ data));

        //4. takeLast
        source = Observable.fromArray(numbers).takeLast(3);
        source.subscribe(data->System.out.println("takeLast(3) values = "+ data));

        //5. skip
        source = Observable.fromArray(numbers).skip(2);
        source.subscribe(data->System.out.println("skip(2) values = "+ data));

        //5. skipLast
        source = Observable.fromArray(numbers).skipLast(2);
        source.subscribe(data->System.out.println("skipLast(2) values = "+ data));
    }
}
