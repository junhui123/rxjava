package Chapter3;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import org.apache.commons.lang3.tuple.Pair;
import java.util.ArrayList;
import java.util.List;

public class QueryParser {
    public static void main(String[] args) {
        List<Pair<String, Integer>> sales = new ArrayList<>();
        //객체를 새로 정의 하지 않고 Pair/Tuple 같은 일반화된 자료 구조를 사용
        sales.add(Pair.of("TV", 2500));
        sales.add(Pair.of("Camera", 300));
        sales.add(Pair.of("TV", 1600));
        sales.add(Pair.of("Phone", 800));

        Maybe<Integer> tvSales = Observable.fromIterable(sales)
                .filter(sale->"TV".equals(sale.getLeft()))
                .map(sale->sale.getRight())
                .reduce((sale1, sale2)-> sale1 + sale2);
        tvSales.subscribe(tot->System.out.println("TV Sales: $"+tot));
    }
}
