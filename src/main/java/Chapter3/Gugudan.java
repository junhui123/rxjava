package Chapter3;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

import java.util.Scanner;

public class Gugudan {
    public static void main(String[] args) {
        plainJava();
        System.out.println();
        reactiveV1();
        System.out.println();
        reactiveV2();
        System.out.println();
        reactiveV3();
    }

    public static void plainJava() {
        Scanner in = new Scanner(System.in);
        System.out.println("Gugudan Input : ");
        int dan = Integer.parseInt(in.nextLine());
        for(int row = 1; row<=9; row++) {
            System.out.println(dan + " * " + row + " = " + dan * row );
        }
    }

    public static void reactiveV1() {
        int dan = scanInputDan();

        Observable<Integer> source = Observable.range(1, 9);
        source.subscribe(row->System.out.println(dan + " * "+ row + " = " + dan * row));
    }

    public static void reactiveV2() {
        int dan = scanInputDan();
        Function<Integer, Observable<String>> gugudan = num ->
                Observable.range(1, 9 )
                          .map(row->dan +" * " + row + " = " + dan * row);
        Observable<String> source = Observable.just(dan).flatMap(gugudan);
        source.subscribe(System.out::println);
    }

    public static void reactiveV3() {
        int dan = scanInputDan();

        //flatMap(Function<T, Observable<U>>, BiFunction<T, U, R>)
        //Function으로 T를 받아 결과 Observable<U> 생성
        //BiFunction에서는 T와 Observable<U>를 적용 한 결과 R 생성
        Observable<String> source = Observable.just(dan)
                    .flatMap(
                             gugu->Observable.range(1, 9),
                             (gugu, i) -> gugu + " * " + i + " = " + gugu*i
                            );
        source.subscribe(System.out::println);
    }

    private static int scanInputDan() {
        Scanner in = new Scanner(System.in);
        System.out.println("Gugudan Input : ");
        int dan = Integer.parseInt(in.nextLine());
        return dan;
    }
}
