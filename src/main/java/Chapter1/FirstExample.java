package Chapter1;

import io.reactivex.Observable;

public class FirstExample {
    /*
    * Observable : 데이터 변화가 발생하는 데이터 소스. 리액티브 프로그래밍 시작점
    * just() : Observable 선언 방식. 원시타입/객체 모두 들어감,  같은 타입으로 최대 10개
    * subscribe() ; Observable 구독. 이 메소드를 호출해야 변화를 구독자에게 발행
    * */
    public void emit() {
        Observable.just("Hello", "RxJava2")
                .subscribe(System.out::println);
        //subscribe(data->System.out.println(data));
    }

    public static void main(String[] args) {
        FirstExample demo  = new FirstExample();
        demo.emit();
    }
}
