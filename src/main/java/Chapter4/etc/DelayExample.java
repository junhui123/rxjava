package Chapter4.etc;

import common.CommonUtils;
import common.Log;
import common.MarbleDiagram;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

import static common.Shape.*;


public class DelayExample implements MarbleDiagram {
	@Override
	public void marbleDiagram() {
		CommonUtils.exampleStart();
		
		String[] data = {RED, ORANGE, YELLOW, GREEN, SKY};
		Observable<String> source = Observable.fromArray(data)
				.delay(100L, TimeUnit.MILLISECONDS);
		source.subscribe(Log::it);
		CommonUtils.sleep(1000);
		CommonUtils.exampleComplete();
	}
	
	public static void main(String[] args) { 
		DelayExample demo = new DelayExample();
		demo.marbleDiagram();
	}
}
