package Chapter4.create;

import common.CommonUtils;
import common.Log;
import common.Shape;
import io.reactivex.Observable;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.Callable;

public class DeferExample {
    private static Iterator<String> colors = Arrays.asList("1", "3", "5", "6").iterator();
    public static void main(String[] args) {
        //timer와 비슷하지만 데이터 흐름 생성을 구독자가 subscribe 호출할 때까지 미룸
        //Observable의 생성이 subscribe 까지 미뤄지기 때문에 최신 데이터 얻을 수 있음
        //subscribe를 호출하면 최신의 데이터 전달
        useDefer();
        System.out.println();
        notDeferred();
    }

    public static void useDefer() {
        Callable<Observable<String>> supplier = ()->getObservable();
        Observable<String> source = Observable.defer(supplier);

        source.subscribe(val->Log.i("Subscriber #1:" + val));
        source.subscribe(val->Log.i("Subscriber #2:" + val));
        CommonUtils.exampleComplete();
    }

    public static void notDeferred() {
        Observable<String> source = getObservable();

        source.subscribe(val -> Log.i("Subscriber #1:" + val));
        source.subscribe(val -> Log.i("Subscriber #2:" + val));
        CommonUtils.exampleComplete();
    }

    private static Observable<String> getObservable() {
        if (colors.hasNext()) {
            String color = colors.next();
            return Observable.just(
                    Shape.getString(color, Shape.BALL),
                    Shape.getString(color, Shape.RECTANGLE),
                    Shape.getString(color, Shape.PENTAGON));
        }

        return Observable.empty();
    }
}
