package Chapter4.create;

import common.CommonUtils;
import common.Log;
import common.OkHttpHelper;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class RepeatExample {
    public static void main(String[] args) {
        //단순 반복
        //ping, heart beat => 서버가 살아 있는지 확인 시 사용

        String[] balls =  {"1", "3", "5"};
        Observable<String> source = Observable.fromArray(balls)
                .repeat(3); //인자를 주지 않으면 무한히 반복

        //doOnComplete는 onComplete 호출 시 동작 설정
        source.doOnComplete(()->Log.d("onComplete"))
                .subscribe(Log::i);

        System.out.println();
        heartBeatV1();
    }

    public static void heartBeatV1() {
        CommonUtils.exampleStart();
        String serverUrl = "https://api.github.com/zen";

        Observable.timer(2, TimeUnit.SECONDS)
                .map(val->serverUrl)
                .map(OkHttpHelper::get)
                .repeat() //동작이 끝난 다음 다시 구독하는 방식으로 동작. 매번 스레드 달라짐
                          //interval() 사용 시 동일 스레드에서 동작
                .subscribe(res -> Log.it("Ping Result : " + res));
        CommonUtils.sleep(10000);
    }
}
