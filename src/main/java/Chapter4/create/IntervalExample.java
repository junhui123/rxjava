package Chapter4.create;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class IntervalExample {
    public static void main(String[] args) {
        //interval() Long을 출력하는 Observable 생성
        //interval()은 무한히 진행. 풀링 방식으로 많이 사용
        printNumbers();
        System.out.println();
        noInitialDelay();
    }

    public static void printNumbers() {
        CommonUtils.exampleStart();
        //다른 스레드에서 실행
        Observable<Long> source = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .map(data->(data+1)*100)
                .take(5);
        source.subscribe(Log::it);
        //메인스레드 종료 방지
        //메인스레드 종료 시 프로그램 종료
        CommonUtils.sleep(1000);
    }

    public static void noInitialDelay(){
        CommonUtils.exampleStart();
        //초기 지연값이 0이기 때문에 시간이 줄어듬
        Observable<Long> source = Observable.interval(0L, 100L, TimeUnit.MILLISECONDS)
                .map(val->val+100)
                .take(5);
        source.subscribe(Log::it);
        CommonUtils.sleep(1000);
    }
}
