package Chapter4.combine;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;
import io.reactivex.functions.Action;

import java.util.concurrent.TimeUnit;

public class ConcatExample {
    public static void main(String[] args) {
        /**
         * 최대 4개의 Observable 결합
         * 입력 Observable이 onComplete 이벤트가 발행사지 않으면
         * 다음 Observable은 영원히 대기 하므로 메모리 릭 위험
         * 입력된 Observable은 반드시 onComplete 필요
         */
        userConcat();
    }

    public static void userConcat(){
        Action onCompleteAction = ()->Log.d("onComplete()");

        String[] data1 = {"1", "3", "5"};
        String[] data2 = {"2", "4", "6"};
        Observable<String> source1 = Observable.fromArray(data1)
                .doOnComplete(onCompleteAction);

        Observable<String> source2= Observable.interval(100L, TimeUnit.MILLISECONDS)
                .map(Long::intValue)
                .map(idx->data2[idx])
                .take(data2.length)
                .doOnComplete(onCompleteAction); //onComplete 이벤트 발생 확인 로그 출력

        Observable<String> source = Observable.concat(source1, source2)
                .doOnComplete(onCompleteAction);
        source.subscribe(Log::i);
        CommonUtils.sleep(1000);



    }
}
