package Chapter4.combine;

import common.CommonUtils;
import common.Log;
import common.Shape;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class CombineLatestExample {
    public static void main(String[] args) {
        useCombineLatest();
    }

    public static void useCombineLatest(){
        //2개 이상의 Observable를 기반으로 Observable 각각이 변경 시 값을 갱신
        //변경된 최신의 Observable 값으로 갱신
        //엑셀의 =A+B 수식에서 A와 B셀 값 변경 방식과 같음
        String[] data1 = {"6", "7", "4", "2"};
        String[] data2 = {"DIAMOND", "STAR", "PENTAGON"};

        Observable<String> source = Observable.combineLatest(
                Observable.fromArray(data1)
                    .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS),
                            (shape, notUsed)->Shape.getColor(shape)),
                Observable.fromArray(data2)
                                        //최초 150ms 쉬고 200ms 간격으로 Long값 발행
                    .zipWith(Observable.interval(150L, 200L, TimeUnit.MILLISECONDS),
                            (shape, notUsesd)->Shape.getSuffix(shape)), (v1, v2) -> v1+v2
        );
        source.subscribe(Log::i);
        CommonUtils.sleep(1000);
    }
}
