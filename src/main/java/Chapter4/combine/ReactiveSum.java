package Chapter4.combine;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

import java.util.Scanner;

public class ReactiveSum {
    public static void main(String[] args) {
        new ReactiveSum().run();
    }

    public void run() {
        /**
         * ConnectableObservable은 차가운 Observable -> 뜨거운 Observable 변경 및 데이터를 동시에 여러 구독자에게 전달
         * 차가운 : fromIterable, just, .... subscribe()를 실행해야 데이터 발행
         * 뜨거운 : subscribe() 상관없이 데이터 발행
         */
        ConnectableObservable<String> source = userInput();
        Observable<Integer> a = source
                .filter(str -> str.startsWith("a:"))
                .map(str -> str.replace("a:", ""))
                .map(Integer::parseInt);

        Observable<Integer> b = source
                .filter(str -> str.startsWith("b:"))
                .map(str -> str.replace("b:", ""))
                .map(Integer::parseInt);

        //처음 값을 발행하려면 a와 b 모두에서 값을 발행해야 하므로 startWith(0)
        Observable.combineLatest(a.startWith(0), b.startWith(0), (x, y) -> x + y)
                .subscribe(res -> System.out.println("Result: " + res));
        source.connect(); //데이터 발행
    }

    private ConnectableObservable<String> userInput() {
        return Observable.create(ReactiveSum::subscribe).publish(); //데이터 발행 유예
    }

    private static void subscribe(ObservableEmitter<String> emiiter) {
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("Input: ");
            String line = in.nextLine();
            emiiter.onNext(line);

            if (line.indexOf("exit") >= 0) {
                in.close();
                break;
            }
        }
    }
}