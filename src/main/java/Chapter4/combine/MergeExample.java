package Chapter4.combine;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class MergeExample {
    public static void main(String[] args) {
        /**
         * merge()는 2개의 Observable의 값을 하나의 Observable로 결합
         * 먼저 입력되는 Observable의 값을 그대로 발행
         */
        String[] data1 = {"1", "3"};
        String[] data2 = {"2", "4", "6"};

        //source1, source2는 각각 다른 스레드에서 데이터 발행
        Observable<String> source1 = Observable.interval(0L, 100L, TimeUnit.MILLISECONDS)
                .map(Long::intValue)
                .map(idx->data1[idx])
                .take(data1.length);

        Observable<String> source2 = Observable.interval(50L, TimeUnit.MILLISECONDS)
                .map(Long::intValue)
                .map(idx->data2[idx])
                .take(data2.length);

        Observable<String> source = Observable.merge(source1, source2);

        source.subscribe(Log::i);
        CommonUtils.sleep(1000);
    }
}
