package Chapter4.combine;

import common.Log;
import io.reactivex.Observable;
import org.apache.commons.lang3.tuple.Pair;

import java.text.DecimalFormat;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class ElectricBill {
    private int index = 0;

    public static void main(String[] args) {
        new ElectricBill().V1();
        System.out.println();
        new ElectricBill().V2();
    }

    public void V1() {
        String[] data = {
                "100",
                "300"
        };

        Observable<Integer> basePrice = Observable.fromArray(data)
                .map(Integer::parseInt)
                .map(val -> {
                    if (val <= 200) return 910;
                    if (val <= 400) return 1600;
                    return 7300;
                });

        Observable<Integer> usagePrice = Observable.fromArray(data)
                .map(Integer::parseInt)
                .map(val -> {
                    double series1 = min(200, val) * 93.3;
                    double series2 = min(200, max(val - 200, 0)) * 187.9;
                    double series3 = max(0, max(val - 400, 0)) * 280.65;
                    return (int) (series1 + series2 + series3);
                });

        Observable<Integer> source = Observable.zip(
                basePrice,
                usagePrice,
                (v1, v2) -> v1 + v2);

        source.map(val -> new DecimalFormat("#,###").format(val))
                .subscribe(val -> {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Usage: " + data[index] + " kwh => ");
                    stringBuilder.append("Price: " + val + "원");
                    Log.i(stringBuilder.toString());
                    index++; //SIDE EFFECT
                });
    }

    public void V2() {
        //SIDE EFFECT index++ 없애는 방법
        //1. data를 추가로 넘겨주는 방법
        //2. zip()에 Observable 추가로 전달
        //3. Pair 클래스 사용

        String[] data = {
                "100",
                "300"
        };

        Observable<Integer> basePrice = Observable.fromArray(data)
                .map(Integer::parseInt)
                .map(val -> {
                    if (val <= 200) return 910;
                    if (val <= 400) return 1600;
                    return 7300;
                });

        Observable<Integer> usagePrice = Observable.fromArray(data)
                .map(Integer::parseInt)
                .map(val -> {
                    double series1 = min(200, val) * 93.3;
                    double series2 = min(200, max(val - 200, 0)) * 187.9;
                    double series3 = max(0, max(val - 400, 0)) * 280.65;
                    return (int) (series1 + series2 + series3);
                });

        Observable<Pair<String, Integer>> source = Observable.zip(
                basePrice,
                usagePrice,
                Observable.fromArray(data),
                (v1, v2, i) -> Pair.of(i, v1 + v2));


        source.map(val->Pair.of(val.getLeft(),
                new DecimalFormat("#,###").format(val.getValue())))
                .subscribe(val-> {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Usage: " + val.getLeft() + " kwh => ");
                    stringBuilder.append("Price: " + val.getRight() + "원");
                    Log.i(stringBuilder.toString());
                });
    }
}
