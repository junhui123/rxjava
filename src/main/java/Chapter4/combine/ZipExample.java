package Chapter4.combine;

import common.CommonUtils;
import common.Log;
import common.Shape;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class ZipExample {
    public static void main(String[] args) {
        useZip();
        System.out.println();
        zipNumbers();
        System.out.println();
        zipInterval();
        System.out.println();
        zipNumbersZipWith();
    }

    public static void useZip(){
        String[] shape = {"BALL", "PENTAGON" , "STAR"};
        String[] coloredTriangles= {"2-T", "6-T", "4-T"};

        //Zip() 2~9개의 Observable을 결합
        //Observable 모두 데이터를 발행해야 결합 가능
        Observable<String> source = Observable.zip(
                Observable.fromArray(shape).map(Shape::getSuffix),
                Observable.fromArray(coloredTriangles).map(Shape::getColor),
                (suffix, color) -> color + suffix);
        source.subscribe(Log::i);
    }

    public static void zipNumbers(){
        Observable<Integer> source = Observable.zip(
          Observable.just(100, 200 ,300),
          Observable.just(10, 20 ,30),
          Observable.just(1, 2 ,3),
          (a,b,c) -> a+b+c
        );
        source.subscribe(Log::i);
    }

    public static void zipInterval() {
        Observable<String> source = Observable.zip(
                Observable.just("RED", "GREEN", "BLUE"),
                Observable.interval(200L, TimeUnit.MILLISECONDS), //200ms 간격으로 Long 생성(0,1,2..)
                (value, i)->value + "<>" + i
        );
        CommonUtils.exampleStart();
        source.subscribe(Log::it);
        CommonUtils.sleep(1000);
    }

    public static void zipNumbersZipWith(){
        Observable<Integer> source = Observable.zip(
                Observable.just(100, 200 ,300),
                Observable.just(10, 20 ,30),
                (a,b)->a+b).zipWith(Observable.just(1,2,3), (ab, c)->ab+c);
        source.subscribe(Log::i);
    }
}
