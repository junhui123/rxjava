package Chapter4.transform;

import common.Shape;
import io.reactivex.Observable;
import io.reactivex.observables.GroupedObservable;

public class GroupBy {
    public static void main(String[] args) {
        /**
         * map() 1개의 데이터를 다른 데이터로 변경
         * flatMap() 1개의 데이터를 여러 개의 Observable로 확장
         * groupBy() 여러 데이터를 기준에 맞는 GroupedObservable 생성
         */
        filterBallGroup();
    }

    public static void filterBallGroup(){
        //groupBy(KeySelector) keySelector를 기준으로 GroupedObservable 생성
        String[] objs = {"6", "4", "2-T", "2", "6-T", "6-T"};
        Observable<GroupedObservable<String, String>> source =
                Observable.fromArray(objs).groupBy(Shape::getShape);

        source.subscribe(obj->{ //obj = GroupedObservable
            obj.filter(val->{
               return obj.getKey().equals(Shape.BALL);
            })
            .subscribe(val-> System.out.println("GRUOP:"+obj.getKey()+ "\t Value:"+val));
        });
    }
}
