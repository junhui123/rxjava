package Chapter4.transform;

import common.Log;
import io.reactivex.Observable;

public class ScanExample {
    public static void main(String[] args) {
        //scan()은 reduce()와 같은 결과를 생성
        //차이점은 reduce()는 중간 과정을 확인 할 수 없고
        //scan()은 중간결과와 최종 결과를 둘 다 확인 가능함
        String[] balls = {"1", "3", "5"};
        Observable<String> source = Observable.fromArray(balls)
                .scan((ball1, ball2)-> ball2 + "("+ball1+")");
        source.subscribe(Log::i);
    }
}
