package Chapter4.transform;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class SwitchMapExample {
    public static void main(String[] args) {
        useSwitchMap();
        System.out.println();
        useDoOnNext();
    }

    public static void useSwitchMap(){
        //1번 스레드는 데이터 발행에 사용되고 이후 나머지 스레드 풀에서 가져와 구독자가 데이터 처리
        //기존의 진행중인 작업을 중단하고 새로운 아이템을 처리
        //중간에 끊기더라도 마지막의 값 처리는 보장
        //센서의 값을 얻어와 처리시 사용. 센싱은 최종값으로 처리하는 경우가 많음
        CommonUtils.exampleStart();
        String[] balls = {"1", "3", "5"};
        Observable<String> source = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .map(Long::intValue)
                .map(idx->balls[idx])
                .take(balls.length)
                .switchMap(ball->Observable.interval(200L, TimeUnit.MILLISECONDS)
                        .map(notUsed->{
                            //System.out.println("notUsed = "+notUsed); 위에서 만든 Long 값
                            //System.out.println("ball = "+ball); 위에서 변환한 ball값
                            return ball+"<>";
                        })
                        .take(2)
                );
        source.subscribe(Log::it);
        CommonUtils.sleep(2000);
    }

    public static void useDoOnNext() {
        CommonUtils.exampleStart();

        String[] balls = {"1", "3", "5"};
        Observable<String> source = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .map(Long::intValue)
                .map(idx->balls[idx])
                .take(balls.length)
                .doOnNext(Log::dt) //중간 결과 확인
                .switchMap(ball->Observable.interval(200L, TimeUnit.MILLISECONDS)
                        .map(notUsed->ball+"<>")
                        .take(2)
                );
        source.subscribe(Log::it);
        CommonUtils.sleep(2000);
    }
}
