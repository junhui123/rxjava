package Chapter4.transform;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class ConcatMapExample {
    public static void main(String[] args) {
        useConcatMap();
        System.out.println();
        interleaving();
    }

    public static void useConcatMap() {
        //concatMap() = FIFO 동작
        //concatMap() 순서를 보장해주기 위한 추가 시간 필요
        //flatMap() = 처리 도중 새로운 데이터 인터리빙(끼어들기) 가능
        CommonUtils.exampleStart();

        String[] balls = {"1", "3", "5"};
        //interval은 Long 출력 Observable 생성
        Observable<String> source = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .map(Long::intValue) //Lo
                .map(idx->balls[idx])
                .take(balls.length)
                .concatMap(ball->Observable.interval(200L, TimeUnit.MILLISECONDS)
                    .map(notUsed->ball+"<>")
                    .take(2)
                );

        source.subscribe(Log::it);
        CommonUtils.sleep(2000);
    }

    public static void interleaving(){
        CommonUtils.exampleStart();
        String[] balls = {"1", "3", "5"};
        Observable<String> source = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .map(Long::intValue) //Lo
                .map(idx->balls[idx])
                .take(balls.length)
                .flatMap(ball->Observable.interval(200L, TimeUnit.MILLISECONDS)
                    .map(notUsed->ball+"<>").take(2)
                );
        source.subscribe(Log::it);
        CommonUtils.sleep(2000);
    }


    private static void testInterval(){
        Observable.interval(1, TimeUnit.SECONDS)
                .subscribe(data->{
                    System.out.print(data.getClass().getSimpleName()+" => ");
                    System.out.println(data);
                });
        CommonUtils.sleep(10000);
    }
}
