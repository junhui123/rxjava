package Chapter4.conditional;


import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class SkipUntilExample {
    public static void main(String[] args) {
        /**
         * takeUntil의 반대
         * 주어진 Other Observable의 데이터 발행 하는 순간
         * 원래 Observable에서 값을 정상 발행
         */
        useSkilUntil();
    }

    public static void useSkilUntil() {
        String[] data = {"1", "2", "3", "4", "5", "6"};

        Observable<String> source = Observable.fromArray(data)
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (val, notUsed)->val)
                .skipUntil(Observable.timer(500L, TimeUnit.MILLISECONDS));

        source.subscribe(Log::i);
        CommonUtils.sleep(1000);
    }
}
