package Chapter4.conditional;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AmbExample {
    public static void main(String[] args) {
        /**
         * 먼저 나오는 Observable을 선택
         */
        useAmb();
    }

    public static void useAmb(){
        String[] data1 = {"1", "3", "5"};
        String[] data2 = {"2-R", "4-R"};

        List<Observable<String>> sources = Arrays.asList(
          Observable.fromArray(data1)
                  .doOnComplete(()->Log.d("Observable #1 : onComplete()")),
          Observable.fromArray(data2)
                  .delay(100L, TimeUnit.MILLISECONDS)
                  .doOnComplete(()->Log.d("Observable #2 : onComplete()"))
        );

        //sources 내부의 Observable #1이 먼저 나오므로 선택됨
        Observable.amb(sources)
                .doOnComplete(()->Log.d("Result: onComplete()"))
                .subscribe(Log::i);
        CommonUtils.sleep(1000);
    }
}
