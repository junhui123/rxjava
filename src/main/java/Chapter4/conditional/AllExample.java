package Chapter4.conditional;


import common.Log;
import common.Shape;
import io.reactivex.Observable;
import io.reactivex.Single;

public class AllExample {
    public static void main(String[] args) {
        /**
         * all(조건)
         * 조건에 부합하는 경우 true를 발행
         * 아닌경우 false를 발행
         */
        useAll();
    }

    public static void useAll() {
        String[] data = {"1", "2", "3", "4"};

        Single<Boolean> source = Observable.fromArray(data)
            .map(Shape::getShape)
            .all(Shape.BALL::equals);
            //.all(val->Shape.BALL.equals(Shape.getShape(val)));

        source.subscribe(b->{
            System.out.println(Log.getThreadName()+ " | value = " + b);
        });
    }
}
