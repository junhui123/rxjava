package Chapter4.conditional;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;

import java.util.concurrent.TimeUnit;

public class TakeUntilExample {
    public static void main(String[] args) {
        /**
         * takeUntil(Other Observable)
         * 기존 Observable이 값을 발행하다가 Other Observable의 값이 발행되며
         * 기존 Observable을 완료(onComplete())함
         */
        useTakeUntil();
    }

    public  static  void useTakeUntil(){
        String[] data = {"1", "2", "3", "4", "5", "6"};

        Observable<String> source = Observable.fromArray(data)
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (val, notUsed)->val)
                .takeUntil(Observable.timer(500L, TimeUnit.MILLISECONDS));

        source.subscribe(Log::i);
        CommonUtils.sleep(1000);
    }
}
