package Chapter5;

import common.CommonUtils;
import common.Log;
import common.Shape;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static common.Shape.*;

public class FlipExample {
    public static void main(String[] args) {
        //useFlip();
        System.out.println();
        basic();
    }

    public static void useFlip(){
        String[] objs = {"1-S", "2-T", "3-P"};
        //비동기 핵심 = 데이터 흐름 발생 스레드와 처리 결과를 구독자에게 전달할 스레드의 분리
        Observable<String> source = Observable.fromArray(objs)
                .doOnNext(data-> Log.v("Original data = " + data))
                .subscribeOn(Schedulers.newThread())//구독자가 subscribe()시 실행될 스레드 지정. 데이터 발행 스레드
                .observeOn(Schedulers.newThread())  //Observable에서 생성한 데이터 처리 흐름시 사용할 스레드로 지정
                                                    //observeOn() 지정하지 않으면 subscribeOn에서 지정한 스레드로 모든 로직 처리
                .map(Shape::flip);
        source.subscribe(Log::i);
        CommonUtils.sleep(500);
    }

    public static void basic() {
        String[] orgs = {RED, GREEN, BLUE};
        Observable.fromArray(orgs)
                .doOnNext(data -> Log.v("Original data : " + data))
                .map(data -> "<<" + data + ">>")
                .subscribeOn(Schedulers.newThread())
                .subscribe(Log::i);
        CommonUtils.sleep(500);

        Observable.fromArray(orgs)
                .doOnNext(data -> Log.v("Original data : " + data))
                .map(data -> "##" + data + "##")
                .subscribeOn(Schedulers.newThread())
                .subscribe(Log::i);
        CommonUtils.sleep(500);
    }
}
