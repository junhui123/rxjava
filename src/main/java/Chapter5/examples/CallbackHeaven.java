package Chapter5.examples;

import common.CommonUtils;
import common.Log;
import common.OkHttpHelper;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static common.CommonUtils.GITHUB_ROOT;

public class CallbackHeaven {
    private static final String FIRST_URL = "https://api.github.com/zen";
    private static final String SECOND_URL = GITHUB_ROOT + "/samples/callback_heaven";

    public void usingConcat() {
        /**
         * 현재의 Observable에 concatWith(Observable)을 결합
         * 현재의 Observable의 데이터 발행이 끝날 때까지 기다려야 함
         *
         * 순수한 비즈니스 로직과 비동기 동작을 위한 코드 구분
         * 가독성의 개선. 정상 로직과 예외 처리 분리
         **/
        CommonUtils.exampleStart();
        Observable<String> source = Observable.just(FIRST_URL)
                .subscribeOn(Schedulers.io())
                .map(OkHttpHelper::get)
                .concatWith(Observable.just(SECOND_URL)
                        .map(OkHttpHelper::get));
        source.subscribe(Log::it);
        CommonUtils.sleep(5000);
    }

    public void usingZip() {
        /**
         * zip(Observable, Observable, BiFunction)
         * URL 요청을 동시에 수행하고 결과만 결합
         *
         * concatWith()에 비해서 빠름..URL을 동시에 요청 하므로..
         */
        CommonUtils.exampleStart();
        Observable<String> first = Observable.just(FIRST_URL)
                .subscribeOn(Schedulers.io())
                .map(OkHttpHelper::get);

        Observable<String> second = Observable.just(SECOND_URL)
                .subscribeOn(Schedulers.io())
                .map(OkHttpHelper::get);

        Observable.zip(first, second, (a,b)->("\n>>"+a+"\n>>"+b)).subscribe(Log::it);
        CommonUtils.sleep(5000);

    }

    public static void main(String[] args) {
        CallbackHeaven heaven = new CallbackHeaven();
        heaven.usingConcat();
        System.out.println();
        heaven.usingZip();
    }
}
