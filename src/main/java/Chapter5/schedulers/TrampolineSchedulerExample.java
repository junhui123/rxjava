package Chapter5.schedulers;


import common.Log;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class TrampolineSchedulerExample {
    public static void main(String[] args) {
        basic();
    }

    public static void basic(){
        /**
         * 트램펄린 스케쥴러 = 새로운 스레드 생성 없음. 현재 스레드에 무한한 크기의 대기 큐를 생성
         * 큐에 작업을 넣고 순차적으로 실행
         */

        String[] orgs = {"1", "3", "5"};
        Observable<String> source = Observable.fromArray(orgs);

        source.subscribeOn(Schedulers.trampoline())
                .map(data->"<<"+data+">>")
                .subscribe(Log::i);

        source.subscribeOn(Schedulers.trampoline())
                .map(data->"##"+data+"##")
                .subscribe(Log::i);
    }
}
