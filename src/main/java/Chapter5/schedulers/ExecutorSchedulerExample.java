package Chapter5.schedulers;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ExecutorSchedulerExample {
    public static void main(String[] args) {
        basic();
    }

    public static void basic(){
        /**
         * Executor 변환 스케쥴러
         * 자바의 Executor를 변환해서 스케쥴러로 사용
         * 기존에 Executor로 사용하던 부분 재사용 시 활용
         */
        final int THREAD_NUM = 10;

        String[] data = {"1", "3", "5"};
        Observable<String> source = Observable.fromArray(data);
        Executor executor = Executors.newFixedThreadPool(THREAD_NUM);

        source.subscribeOn(Schedulers.from(executor))
                .subscribe(Log::i);

        source.subscribeOn(Schedulers.from(executor))
                .subscribe(Log::i);

        CommonUtils.sleep(500);
    }
}
