package Chapter5.schedulers;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.io.File;

public class IOSchedulerExample {
    public static void main(String[] args) {
        basic();
    }

    public static void basic(){
        /**
         * IO 스케쥴러 = 네트워크 작업, 파일 입/출력, DB 쿼리 수행 등 입력/출력 작업 수행
         * 기본으로 생성되는 스레드 개수가 다름. IO는 필요시 마다 스레드를 계속 생성
         * 입/출력 작업은 비동기로 실행되나 대기 시간이 오래 걸림
         */
        String root = "c:\\";

        File[] files = new File(root).listFiles();
        Observable<String> source = Observable.fromArray(files)
                .filter(f->!f.isDirectory())
                .map(f->f.getAbsolutePath())
                .subscribeOn(Schedulers.io());

        source.subscribe(Log::i);
        CommonUtils.sleep(500);
    }
}
