package Chapter5.schedulers;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class SingleSchedulerExample {
    public static void main(String[] args) {
        basic();
    }

    public static void basic(){
        /**
         * 싱글 스레드 스케쥴러 = 단일 스레드 별도로 생성하여 구독 작업에 사용. 여러 번 구독 요청시에도 공통 사용
         * 비동기 지향 이므로 싱글 스레드 활용할 확률은 낮음
         */
        Observable<Integer> numbers = Observable.range(100, 5);
        Observable<String> chars = Observable.range(0, 5)
                .map(CommonUtils::numberToAlphabet);

        numbers.subscribeOn(Schedulers.single())
                .subscribe(Log::i);

        chars.subscribeOn(Schedulers.single())
                .subscribe(Log::i);

        CommonUtils.sleep(500);
    }
}
