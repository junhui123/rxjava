package Chapter5.schedulers;

import common.CommonUtils;
import common.Log;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class ComputationSchedulerExample {
    public static void main(String[] args) {
        basic();
    }

    public static void basic(){
        /**
         * 계산 스케쥴러 = 입출력을 하지 않는 작업을 담당하는 스레드
         * 내부적으로 프로세서수와 동일한 스레드풀을 생성
         *
         * 1번째 구독/2번째 구독이 거의 동시에 이뤄지므로
         * 같은 스레드에 작업이 할당되기도 함
         */
        String[] objs = {"1", "3", "5"};
        Observable<String> source = Observable.fromArray(objs)
                .zipWith(Observable.interval(100L, TimeUnit.MILLISECONDS), (a,b)->a);

        //1번째 구독
        source.map(item -> "<<" + item + ">>")
                .subscribeOn(Schedulers.computation())
                .subscribe(Log::i);

        //2번째 구독
        source.map(item -> "##" + item + "##")
                .subscribeOn(Schedulers.computation())
                .subscribe(Log::i);

        CommonUtils.sleep(1000);
    }
}
