package Chapter2.ConnectableObserable;

import common.CommonUtils;
import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

import java.util.concurrent.TimeUnit;

public class ConnectableObservableExample {
    public static void main(String[] args) {
        //차가운 Observable을 뜨거운으로 변환
        //데이터를 여러 구독자에게 동시에 전달 시 사용
        //Observable은 자동적으로 emit, ConnectableObservable은 수동적. connect()를 호출해야 emit

        //여러개의 구독자가 있더라도 한 번의 연산으로 거의 동시에 같은 값을 사용함
        //connect() 시점이 중요

        String[] dt = {"1", "3", "5"};
        Observable<String> balls = Observable.interval(100L, TimeUnit.MILLISECONDS)
                .map(Long::intValue)
                .map(i->dt[i])
                .take(dt.length);

        //publish는 connect 호출전까지 데이터 발행 유예, Observable->ConnectableObservable로 변경
        ConnectableObservable<String> source = balls.publish();
        source.subscribe(data->System.out.println("Subscriber #1 => "+ data));
        source.subscribe(data->System.out.println("Subscriber #2 => "+ data));
        //subscribe는 구독 등록 connect를 호출해야 구독자에게 데이터를 동시에 발행.
        source.connect();


        CommonUtils.sleep(250);
        source.subscribe(data->System.out.println("Subscriber #3 => "+ data));
        CommonUtils.sleep(100);
    }
}
