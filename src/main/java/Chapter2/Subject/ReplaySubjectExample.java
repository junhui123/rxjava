package Chapter2.Subject;

import io.reactivex.subjects.ReplaySubject;

public class ReplaySubjectExample {
    public static void main(String[] args) {
        //뜨거운 Observable을 차가운것처럼 사용
        //구독자가 생기면 항상 데이터의 처음부터 끝까지 발행하는 것을 보장
        //데이터를 모두 저장하는 과정에서 메모리 릭 발생 가능
        ReplaySubject<String> subject = ReplaySubject.create();
        subject.subscribe(data->System.out.println("Subscriber #1 => "+ data));
        subject.onNext("1");
        subject.onNext("3");
        subject.subscribe(data->System.out.println("Subscriber #2 => "+ data));
        subject.onNext("5");
        subject.onComplete();

    }
}
