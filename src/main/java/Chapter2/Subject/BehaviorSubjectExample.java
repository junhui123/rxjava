package Chapter2.Subject;

import io.reactivex.subjects.BehaviorSubject;

public class BehaviorSubjectExample {
    public static void main(String[] args) {
        //가장 최근 값 혹은 기본값을 구독자에게 넘겨 주는 클래스

        BehaviorSubject<String> subject = BehaviorSubject.createDefault("6");
        subject.subscribe(data->System.out.println("Subscriber #1 => " + data));
        subject.onNext("1");
        subject.onNext("3");
        subject.subscribe(data->System.out.println("Subscriber #2 => " + data));
        subject.onNext("5");
        subject.onComplete();
    }
}
