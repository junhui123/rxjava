package Chapter2.Subject;

import io.reactivex.Observable;
import io.reactivex.subjects.AsyncSubject;

public class AsyncSubjectExample {
    public static void main(String[] args) {
        AsyncSubject<String> subject = AsyncSubject.create();
        subject.subscribe(data->System.out.println("Subscriber #1 => " + data));
        subject.onNext("1");
        subject.onNext("3");
        subject.subscribe(data->System.out.println("Subscriber #2 => " + data));
        subject.onNext("5");
        subject.onComplete();

        Float[] temprature = {10.1f, 13.4f, 12.5f};
        Observable<Float> source = Observable.fromArray(temprature);

        //abstract class Subject<T> extends Observable<T> implements Observer<T>
        //Subject는 Observable, Observer 두가지 특징을 모두 가짐
        AsyncSubject<Float> floatAsyncSubject = AsyncSubject.create();
        floatAsyncSubject.subscribe(data->System.out.println("Subscriber #1 => " + data));
        source.subscribe(floatAsyncSubject);

        //onComplete() 이후 onNext("13")은 작동 안함
        AsyncSubject<Integer> integerAsyncSubject = AsyncSubject.create();
        integerAsyncSubject.onNext(10);
        integerAsyncSubject.onNext(11);
        integerAsyncSubject.subscribe(data->System.out.println("Subscriber #1 => " + data));
        integerAsyncSubject.onNext(12);
        integerAsyncSubject.onComplete();
        integerAsyncSubject.onNext(13);
        integerAsyncSubject.subscribe(data->System.out.println("Subscriber #2 => " + data));
        integerAsyncSubject.subscribe(data->System.out.println("Subscriber #3 => " + data));
    }
}
