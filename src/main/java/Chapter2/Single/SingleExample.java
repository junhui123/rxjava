package Chapter2.Single;

import common.Order;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.util.function.Consumer;

public class SingleExample {
    public static void main(String[] args) {
        //Single? Observable? ?? ?? 1?? ???? ??
        //??? ??? ???? ?? API ??? ??

        //1. Observable to Single
        Observable<String> source = Observable.just("Hello Single");
        Single.fromObservable(source).subscribe(System.out::println);

        //2. call single()
        Observable.just("Hello Single").single("default item").subscribe(System.out::println);

        //3. call first()
        String[] colors = {"Red", "Blue", "Gold"};
        Observable.fromArray(colors)
                .first("default Color")
                .subscribe(System.out::println);

        //4. empty Observable to Single
        Observable.empty().single("default value")
                .subscribe(System.out::println);

        //5. take()
        Observable.just(new Order("ORD-1"), new Order("ORD-2"))
                .take(1)
                .single(new Order("default order"))
                .subscribe(System.out::println);


        /*
         //Single? ??? ?? ??? ??
         Single<String> singleSource = Observable.just("Hello Single", "Error")
                .single("default Item")
                .subscribe(System.out::println);
        */

        Single<String> single = Single.just("Hello Single");
        single.subscribe(System.out::println);

        Single.just("Hello Single 2").subscribe(System.out::println);
    }
}
