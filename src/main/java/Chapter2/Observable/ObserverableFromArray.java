package Chapter2.Observable;

import io.reactivex.Observable;

import java.util.stream.IntStream;

public class ObserverableFromArray {
    public static void main(String[] args) {
        Integer[] arr = {100, 200, 300};
        Observable<Integer> source = Observable.fromArray(arr);
        source.subscribe(System.out::println);

        int[] intArray = {400, 500, 600};
        //원시형은 Wrap 필요..
        Observable.fromArray(intArray).subscribe(System.out::println);
        Observable.fromArray(toIntegerArray(intArray)).subscribe(System.out::println);
    }

    private static Integer[] toIntegerArray(int[] intArray){
        return IntStream.of(intArray).boxed().toArray(Integer[]::new);
    }
}
