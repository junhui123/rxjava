package Chapter2.Observable;

import common.Order;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ObservableFromIterable {
	public static void main(String[] args) {
		List<String> names = new ArrayList<>();
		names.add("Jerry");
		names.add("William");
		names.add("Bob");
		
		Observable<String> source = Observable.fromIterable(names);
		source.subscribe(System.out::println);

		Set<String> cities  = new HashSet<>();
		cities.add("Seoul");
		cities.add("London");
		cities.add("Paris");
		source = Observable.fromIterable(cities);
		source.subscribe(System.out::println);

		BlockingQueue<Order> orderBlockingQueue = new ArrayBlockingQueue<>(100);
		Observable<Order> sourceOrder = Observable.fromIterable(orderBlockingQueue);
		sourceOrder.subscribe(order->System.out.println(order.getId()));
	}
}
